"use client";
import React, { ForwardRefExoticComponent } from "react";
import { Flex, Heading, Icon, Stack, Text } from "@chakra-ui/react";
import {
  BookOpenText,
  Code2,
  LucideProps,
  Network,
  Settings,
  Smartphone,
  UserRoundCheck,
} from "lucide-react";
import { SectionContainer } from "@/components/common/SectionContainer";

interface SkillsProps {}

type Skill = {
  icon: ForwardRefExoticComponent<LucideProps>;
  title: string;
};

const skills: Skill[] = [
  {
    icon: Code2,
    title: "Web Development",
  },
  {
    icon: UserRoundCheck,
    title: "UI/UX Design",
  },
  {
    icon: Settings,
    title: "API Development",
  },
  {
    icon: Network,
    title: "Web3 Integration",
  },
  {
    icon: Smartphone,
    title: "Mobile App",
  },
  {
    icon: BookOpenText,
    title: "Documentation",
  },
];

export const Skills: React.FC<SkillsProps> = () => {
  return (
    <SectionContainer id={"skills"}>
      <Flex
        flexDirection={"column"}
        justifyContent={"center"}
        alignItems={"center"}
        textAlign={"center"}
      >
        <Text
          mb={3}
          color={"yellow.500"}
          fontWeight={"bold"}
          textDecor={"underline"}
        >
          Skills
        </Text>
        <Heading>What I Am Great At</Heading>
        <Text maxW={{ base: "none", md: "50vw" }}>
          Building Tomorrow's Web: Where Smart Design Meets Modern Tech. I'm here to make web experiences that are not only innovative but also intuitive and user-friendly. Let's collaborate to make something amazing.
        </Text>
      </Flex>
      <Stack
        direction={{ base: "column", md: "row" }}
        flexWrap={"wrap"}
        mt={6}
        gap={8}
        justify={"center"}
        maxW={"1000px"}
      >
        {skills.map((skill) => (
          <Flex
            key={skill.title}
            justifyContent={"center"}
            alignItems={"center"}
            background={"gray.700"}
            boxSize={"300px"}
            flexDirection={"column"}
            p={2}
            position={"relative"}
          >
            <Icon boxSize={"80px"} as={skill.icon} />
            <Heading mt={3} size={"md"}>
              {skill.title}
            </Heading>
          </Flex>
        ))}
      </Stack>
    </SectionContainer>
  );
};
