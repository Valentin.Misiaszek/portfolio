import React from "react";
import { Flex, FlexProps } from "@chakra-ui/react";

interface SectionContainerProps extends FlexProps {}

export const SectionContainer: React.FC<SectionContainerProps> = ({
  children,
  ...rest
}) => {
  return (
    <Flex
      as={"section"}
      flexDirection={"column"}
      justifyContent={"space-between"}
      alignItems={"center"}
      w={"100%"}
      py={"3vh"}
      px={"5vw"}
      mt={2}
      {...rest}
    >
      {children}
    </Flex>
  );
};
