import React from "react";
import { Button, Flex, Heading, Img, Link, Text } from "@chakra-ui/react";

interface HomeProps {}

export const Home: React.FC<HomeProps> = () => {
  return (
    <Flex
      as={"section"}
      flexDirection={{ base: "column", md: "row" }}
      justifyContent={"space-between"}
      alignItems={"center"}
      w={"100%"}
      h={"92vh"}
      py={"3vh"}
      px={"5vw"}
      mt={"8vh"}
      id={"home"}
    >
      <Flex
        flexDirection={"column"}
        alignItems={"center"}
        textAlign={"center"}
        width={{ base: "auto", md: "33%" }}
      >
        <Heading size={"2xl"} flexWrap={"wrap"} as={"h1"} w={"250px"}>
          Valentin Misiaszek
        </Heading>
        <Link href={"#about"} mt={3} display={{ base: "none", md: "block" }}>
          <Button
            variant={"outline"}
            colorScheme={"yellow"}
            color={"yellow.500"}
          >
            Contact me
          </Button>
        </Link>
      </Flex>
      <Img
        alt={"Valentin Misiaszek"}
        src={"./me.png"}
        boxSize={{ base: "180px", md: "350px" }}
        borderRadius={"100%"}
      />
      <Flex
        flexDirection={"column"}
        align={"center"}
        textAlign={"center"}
        width={{ base: "auto", md: "33%" }}
      >
        <Text color={"yellow.500"} fontWeight={"bold"}>
          Introduction
        </Text>
        <Heading size={"2xl"}>Software Fullstack Developer</Heading>
        <Text mt={2} px={5}>
          Driven by new technologies, I specialize in creating innovative web
          experiences. Let's shape the web's future!
        </Text>
        <Link href={"#about"} display={{ base: "block", md: "none" }} mt={3}>
          <Button
            variant={"outline"}
            colorScheme={"yellow"}
            color={"yellow.500"}
          >
            Contact me
          </Button>
        </Link>
        <Link
          mt={2}
          color={"yellow.500"}
          display={{ base: "none", md: "block" }}
          href={"#about"}
        >
          Learn more
        </Link>
      </Flex>
    </Flex>
  );
};
