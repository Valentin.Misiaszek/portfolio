import React from "react";
import { Box, Flex, Text } from "@chakra-ui/react";

interface FooterProps {}

export const Footer: React.FC<FooterProps> = () => {
  return (
    <Flex
      as={"footer"}
      bgColor={"grey.800"}
      w={"100%"}
      h={"15vh"}
      justifyContent={"center"}
      alignItems={"center"}
      borderLeftWidth={"15px"}
      borderRightWidth={"15px"}
      borderColor={"yellow.500"}
    >
      <Box>
        <Text>Designed and built by Valentin Misiaszek</Text>
        <Text mt={2}>©Copyright 2024 - Valentin Misiaszek</Text>
      </Box>
    </Flex>
  );
};
