"use client";
import React from "react";
import {
  CloseButton,
  Divider,
  Flex,
  Heading,
  HStack,
  IconButton,
  Link,
  useDisclosure,
  useMediaQuery,
  VStack,
} from "@chakra-ui/react";
import { MenuIcon } from "lucide-react";

interface HeaderProps {}

export const Header: React.FC<HeaderProps> = () => {
  const [isMobileView] = useMediaQuery("(max-width: 768px)");
  const { isOpen, onOpen, onClose } = useDisclosure();

  return (
    <Flex
      as="header"
      py={3}
      pl={5}
      pr={2}
      justify={"space-between"}
      alignItems={"center"}
      w={"100%"}
      h={"8vh"}
      position={"fixed"}
      zIndex={3}
      background={"gray.800"}
    >
      <Flex alignItems={"baseline"}>
        <Heading>VM</Heading>
        <Heading color={"yellow.500"} fontSize={"2.5em"}>
          .
        </Heading>
      </Flex>
      {isMobileView ? (
        <HStack>
          <IconButton
            aria-label={"menu"}
            icon={<MenuIcon />}
            onClick={onOpen}
          />
          {isOpen && (
            <Flex
              position={"absolute"}
              top={0}
              right={0}
              left={0}
              w={"100%"}
              h={"100vh"}
              background={"gray.800"}
              zIndex={"3"}
              alignItems={"center"}
              justifyContent={"center"}
              flexDirection={"column"}
            >
              <CloseButton
                position={"absolute"}
                top={3}
                right={3}
                onClick={onClose}
              />
              <VStack gap={10} divider={<Divider />}>
                <Links />
              </VStack>
            </Flex>
          )}
        </HStack>
      ) : (
        <HStack gap={5} pr={2}>
          <Links />
        </HStack>
      )}
    </Flex>
  );
};

function Links() {
  const links = ["Home", "About", "Skills", "Experience"];
  return (
    <>
      {links.map((link) => (
        <Link color={"white"} key={link} href={`#${link.toLowerCase()}`}>
          {link}
        </Link>
      ))}
    </>
  );
}
