import React from "react";
import { Box, Button, Flex, Link, Text, VStack } from "@chakra-ui/react";
import { SectionContainer } from "@/components/common/SectionContainer";
import { ExpCard } from "@/components/Experience/ExpCard";

interface ExperienceProps {}

export type ExperienceType = {
  date: string;
  title: string;
  company: string;
  description: string[];
  projects?: { name: string; link: string }[];
  isFreelance: boolean;
};

const experiences: ExperienceType[] = [
  {
    date: "2022 - 2024",
    title: "Open Source Software Developer",
    company: "Gaia-X",
    description: [
      "At Gaia-X, I worked on the Gaia-X Wizard, enhancing users and companies onboarding with Verifiable Credentials and cryptographic signatures.",
      "I also contributed to the Gaia-X framework's website, showcasing its objectives and features. My involvement in implementing a CI/CD pipeline for the official documentation further ensured streamlined maintenance.",
      "Additionally, I honed my open-source skills, actively engaging in community meetings, contributing to collaborative projects, and significantly improving my English communication abilities. These experiences were integral to my professional development and the advancement of the Gaia-X ecosystem.",
    ],
    projects: [
      { name: "Gaia-X Wizard", link: "https://wizard.lab.gaia-x.eu/" },
      { name: "Gaia-X Documentation page", link: "https://docs.gaia-x.eu/" },
      {
        name: "Gaia-X Framework page",
        link: "https://docs.gaia-x.eu/framework/",
      },
    ],
    isFreelance: true,
  },
  {
    date: "2021 - 2022",
    title: "Web Developer Fullstack",
    company: "Decathlon",
    description: [
      "At Decathlon, I contributed to the development of the NEMO application. This internal tool was designed specifically for marketing personnel, aiding them in the creation of landing pages through a user-friendly drag-and-drop interface. It featured generic components and forms to streamline content management.",
    ],
    isFreelance: true,
  },
  {
    date: "2019 - 2021",
    title: "Front-end Developer",
    company: "Leroy Merlin",
    description: [
      "At Leroy Merlin, I was involved with three distinct teams. In the SEO team, I developed a dashboard application to monitor and manage indexed pages. Additionally, I crafted generic components for creating specialized landing pages with A/B testing capabilities. My contributions extended to the core team, responsible for maintaining Leroy Merlin's common library within the development environment.",
      "Within the product team, my focus was on the product listing and product page content, where I implemented several new features.",
      "Subsequently, I joined the search team, which managed the site's custom search engine. There, I developed an application to enhance the relevance of product results for search queries. This involved enabling third-party partners to assess the pertinence of the search results.",
      "Furthermore, I created a User Feedback module, aimed at gauging customer satisfaction throughout their search experience.",
    ],
    isFreelance: false,
  },
  {
    date: "2019",
    title: "Web Developer Fullstack",
    company: "Goweb",
    description: [
      "At Goweb, I worked on web agency projects, developing websites and web applications for various clients. I was involved in the entire development process, from the initial client meeting to the final delivery. This included the design, development, and deployment of the projects.",
      "I also contributed to the company's internal projects",
    ],
    projects: [
      { name: "Oh Fioul", link: "https://oh-fioul.fr/" },
      { name: "Goweb", link: "https://www.goweb.fr/" },
    ],
    isFreelance: false,
  },
];

export const Experience: React.FC<ExperienceProps> = () => {
  return (
    <SectionContainer id={"experience"} bgColor={"gray.700"}>
      <Flex direction={"column"} alignItems={"center"}>
        <Text
          mb={3}
          color={"yellow.500"}
          fontWeight={"bold"}
          textDecor={"underline"}
        >
          Experience
        </Text>
        <Box mt={3} position={"relative"}>
          <VStack spacing={"50px"}>
            {experiences.map((experience) => (
              <ExpCard key={experience.title} experience={experience} />
            ))}
          </VStack>
          <Box
            as={"span"}
            w={"2px"}
            h={"100%"}
            bgColor={"white"}
            position={"absolute"}
            left={"50%"}
            top={"0"}
            zIndex={1}
          />
        </Box>
      </Flex>
      <Link href={"#about"} mt={6}>
        <Button variant={"outline"} colorScheme={"yellow"} color={"yellow.500"}>
          Contact me
        </Button>
      </Link>
    </SectionContainer>
  );
};
