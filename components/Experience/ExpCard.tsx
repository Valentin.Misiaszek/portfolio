"use client";
import React from "react";
import {
  Box,
  Collapse,
  Flex,
  Icon,
  Link,
  Tag,
  Text,
  useDisclosure,
} from "@chakra-ui/react";
import { ExperienceType } from "@/components/Experience/Experience";
import { ArrowUpIcon, MoreHorizontal } from "lucide-react";

interface ExpCardProps {
  experience: ExperienceType;
}

export const ExpCard: React.FC<ExpCardProps> = ({ experience }) => {
  const { isOpen, onToggle } = useDisclosure();

  return (
    <Box
      bgColor={"yellow.500"}
      key={experience.title}
      w={{ base: "auto", md: "70%" }}
      zIndex={2}
    >
      <Flex
        key={experience.company}
        direction={{ base: "column", md: "row" }}
        justifyContent={"space-between"}
        alignItems={"center"}
        p={6}
        bgColor={"gray.800"}
        borderTopLeftRadius={"70px"}
        borderBottomRightRadius={"70px"}
        position={"relative"}
        textAlign={"center"}
      >
        <Flex direction={"column"} alignItems={"center"} w={"50%"}>
          <Text color={"yellow.500"} fontWeight={"bold"}>
            {experience.date}
          </Text>
          <Flex direction={"column"} alignItems={"center"}>
            <Text mt={3} fontWeight={"bold"}>
              {experience.title}
            </Text>
            <Text mt={3} decoration={"underline"}>
              {experience.company}
            </Text>
          </Flex>
        </Flex>
        <Flex
          direction={"column"}
          onClick={onToggle}
          alignItems={"center"}
          w={{ base: "100%", md: "50%" }}
        >
          <Collapse startingHeight={85} in={isOpen}>
            {experience.description.map((desc, index) => (
              <Text key={index} mt={4}>
                {desc}
              </Text>
            ))}
            {experience.projects && (
              <Flex direction={"column"} alignItems={"center"}>
                <Text mt={4} fontWeight={"bold"}>
                  Public Projects
                </Text>
                {experience.projects.map((project, index) => (
                  <Link key={index} href={project.link} isExternal={true}>
                    <Text mt={4} decoration={"underline"}>
                      {project.name}
                    </Text>
                  </Link>
                ))}
              </Flex>
            )}
          </Collapse>
          {!isOpen ? (
            <Icon mt={4} as={MoreHorizontal} />
          ) : (
            <Icon mt={4} as={ArrowUpIcon} />
          )}
        </Flex>
        {experience.isFreelance && (
          <Tag
            position={"absolute"}
            top={2}
            right={2}
            colorScheme={"yellow"}
            color={"yellow.500"}
          >
            Freelance
          </Tag>
        )}
      </Flex>
    </Box>
  );
};
