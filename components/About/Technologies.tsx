"use client";
import React from "react";
import { Tag, TagLabel, TagLeftIcon, Wrap, WrapItem } from "@chakra-ui/react";
import { FaNodeJs, FaReact } from "react-icons/fa";
import { SiNestjs, SiNextdotjs, SiSolidity } from "react-icons/si";
import {TbBrandAdonisJs, TbBrandDocker, TbBrandTypescript} from "react-icons/tb";
import { FaArrowsSpin } from "react-icons/fa6";

interface TechnologiesProps {}

const technologies = [
  {
    name: "React",
    icon: FaReact,
  },
  {
    name: "Next.js",
    icon: SiNextdotjs,
  },
  {
    name: "TypeScript",
    icon: TbBrandTypescript,
  },
  {
    name: "Node.js",
    icon: FaNodeJs,
  },
  {
    name: "Solidity",
    icon: SiSolidity,
  },
  {
    name: "Docker",
    icon: TbBrandDocker,
  },
  {
    name: "CI/CD",
    icon: FaArrowsSpin,
  },
  {
    name: "Nest.js",
    icon: SiNestjs,
  },
  {
    name: "Adonis.js",
    icon: TbBrandAdonisJs ,
  }
];

export const Technologies: React.FC<TechnologiesProps> = () => {
  return (
    <Wrap spacing={3} mt={8} justify={"center"}>
      {technologies.map((tech) => (
        <WrapItem key={tech.name}>
          <Tag
            size={"lg"}
            key={tech.name}
            variant="subtle"
            colorScheme="yellow"
            color={"yellow.500"}
          >
            <TagLeftIcon boxSize="12px" as={tech.icon} />
            <TagLabel>{tech.name}</TagLabel>
          </Tag>
        </WrapItem>
      ))}
    </Wrap>
  );
};
