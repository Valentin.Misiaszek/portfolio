import React from "react";
import {
  Box,
  Button,
  Flex,
  HStack,
  Img,
  List,
  ListItem,
  Text,
} from "@chakra-ui/react";
import { SectionContainer } from "@/components/common/SectionContainer";
import { Socials } from "@/components/About/Socials";
import { Technologies } from "@/components/About/Technologies";

interface AboutProps {}

const aboutInfos: Record<string, string | number> = {
  name: "Valentin Misiaszek",
  age: 27,
  location: "Lille, France",
  email: "dev@misiaszek-valentin.com",
};

export const About: React.FC<AboutProps> = () => {
  return (
    <SectionContainer h={"auto"} id={"about"} bgColor={"gray.700"}>
      <Flex
        flexDirection={"column"}
        justifyContent={"center"}
        textAlign={"center"}
      >
        <Text
          mb={3}
          color={"yellow.500"}
          fontWeight={"bold"}
          textDecor={"underline"}
        >
          About me
        </Text>
        <Flex
          direction={{ base: "column", md: "row-reverse" }}
          mt={5}
          justifyContent={{ base: "space-around", md: "none" }}
          alignItems={"center"}
        >
          <Flex direction={"column"} w={{ base: "auto", md: "50%" }} px={4}>
            <Text mt={{ base: 0, md: 6 }}>
              Hello I am Valentin Misiaszek, I am a Software Developer based in
              Lille, France
            </Text>
            <List>
              {Object.entries(aboutInfos).map(([key, value]) => (
                <ListItem key={key} my={2}>
                  <Flex justifyContent={"space-between"}>
                    <Text fontWeight={"bold"}>{key}: </Text>
                    <Text>{value}</Text>
                  </Flex>
                </ListItem>
              ))}
              <ListItem my={2}>
                <Flex justifyContent={"space-between"} alignItems={"center"}>
                  <Text fontWeight={"bold"}>languages: </Text>
                  <HStack spacing={3}>
                    <Img
                      src={"./fr-flag.png"}
                      objectFit="cover"
                      alt={"France flag"}
                      h={"30px"}
                      w={"50px"}
                    />
                    <Img
                      alt={"Great britain flag"}
                      src={"./gb-flag.png"}
                      objectFit="cover"
                      h={"30px"}
                      w={"50px"}
                    />
                  </HStack>
                </Flex>
              </ListItem>
            </List>
            <Socials />
            <Technologies />
          </Flex>
          <Box px={4}>
            <Box
              borderColor={"yellow.500"}
              borderBottomWidth={"8px"}
              borderLeftWidth={"8px"}
              mt={{ base: 10, md: 0 }}
            >
              <Img
                alt={"Valentin Misiaszek"}
                src={"./me.png"}
                boxSize={{ base: "300px", md: "400px" }}
              />
            </Box>
          </Box>
        </Flex>
      </Flex>

      <HStack gap={5} mt={12}>
        <Button variant={"outline"} color={"yellow.500"} colorScheme={"yellow"}>
          Download CV
        </Button>
      </HStack>
    </SectionContainer>
  );
};
