"use client";
import React from "react";
import { HStack, Icon, IconButton, Link } from "@chakra-ui/react";
import { Gitlab, Linkedin } from "lucide-react";

interface SocialsProps {}

export const Socials: React.FC<SocialsProps> = () => {
  return (
    <HStack mt={3} spacing={5} justify={"center"}>
      <Link isExternal href={"https://gitlab.com/Valentin.Misiaszek"}>
        <IconButton
          colorScheme={"orange"}
          aria-label={"github"}
          icon={<Icon as={Gitlab} />}
        />
      </Link>
      <Link
        isExternal
        href={"https://www.linkedin.com/in/valentin-misiaszek-36475a176"}
      >
        <IconButton
          colorScheme={"linkedin"}
          aria-label={"linkedin"}
          icon={<Icon as={Linkedin} />}
        />
      </Link>
    </HStack>
  );
};
