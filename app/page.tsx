import { Flex } from "@chakra-ui/react";
import { Home } from "@/components/Home";
import { Skills } from "@/components/Skills";
import { Experience } from "@/components/Experience/Experience";
import { About } from "@/components/About/About";
import {Metadata} from "next";

export const metadata: Metadata = {
    title: 'Valentin Misiaszek Portfolio',
}

export default function Page() {
  return (
    <Flex flexDirection={"column"}>
      <Home />
      <About />
      <Skills />
      <Experience />
    </Flex>
  );
}
